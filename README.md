# the-runner
student game project for intro to programming class

HOW TO INSTALL

Python 3.6
and Pygame module required to play.

HOW TO PLAY

Press the up arrow to do a full jump.
Press the left arrow to do a half jump.

CREDITS

DESIGN & PROGRAMMING

Jason

Zakk

Christine


CHARACTER & ENVIORNMENT ART

Christine


SOUND & MUSIC

"Mortal Machine" by Intracate / Newgrounds Audio Portal / Attribution-ShareAlike 3.0 Unported
https://www.newgrounds.com/audio/listen/735541

"Spin Jump" by Brandino480 / soundbible.com / Public Domain
http://soundbible.com/1898-Spin-Jump.html

"Explosion, metal debris" by Blastwave FX / ZapSplat / Standard License
https://www.zapsplat.com/music/explosion-metal-debris/



REFERENCES

General PyGame tutorial
http://www.nerdparadise.com/programming/pygame

Display text
https://pythonprogramming.net/displaying-text-pygame-screen/

Moving a sprite
https://stackoverflow.com/questions/16183265/how-to-move-sprite-in-pygame

Sprite group management
https://stackoverflow.com/questions/20065430/how-to-delete-rects-sprites-with-python-and-pygame
https://stackoverflow.com/questions/40632424/pygame-remove-a-single-sprite-from-a-group

Sprite image tiling
https://stackoverflow.com/questions/16280608/pygame-how-to-tile-subsurfaces-on-an-image
https://stackoverflow.com/questions/35333669/how-can-i-use-tiling-in-a-game-with-python-and-pygame
